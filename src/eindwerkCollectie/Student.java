package eindwerkCollectie;

public class Student{
    private String familienaam;
    private String voornaam;
    private String studentenCode;

    public Student(String familienaam, String voornaam) {
        this.familienaam = familienaam;
        this.voornaam = voornaam;
    }

    public String getFamilienaam() {
        return familienaam;
    }

    public void setFamilienaam(String familienaam) {
        this.familienaam = familienaam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getStudentenCode() {
        return studentenCode;
    }

    public void setStudentenCode(String studentenCode) {
        this.studentenCode = studentenCode;
    }

}


