package eindwerkCollectie;

/**
 *
 * @author Peter
 */
public interface EindwerkCollectieInterface {

    Eindwerk[] getEindwerkenVanOpleiding(String opleiding);

    void verwijder(Eindwerk eindwerk);

    void voegToe(Eindwerk eindwerk);

}