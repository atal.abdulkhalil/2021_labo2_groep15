package eindwerkCollectie;

import java.util.*;

public class EindwerkCollectieTreeMap implements EindwerkCollectieInterface{
    private TreeMap<Integer, Eindwerk> Eindwerken = new TreeMap<Integer, Eindwerk>();
    private Integer counter;

    public EindwerkCollectieTreeMap() {
        counter = 0;
    }



    @Override
    public Eindwerk[] getEindwerkenVanOpleiding(String opleiding) {
       List<Eindwerk> eindwerkenList = new ArrayList<Eindwerk>();

        for (Eindwerk eindwerk : Eindwerken.values()) {
            if (opleiding.equals(eindwerk.getOpleiding())) {
                eindwerkenList.add(eindwerk);
            }
        }

        Eindwerk[] eindwerkenArray = new Eindwerk[eindwerkenList.size()];
        eindwerkenArray = eindwerkenList.toArray(eindwerkenArray);
        return eindwerkenArray;
    }

    @Override
    public void verwijder(Eindwerk eindwerk) {
        Set<Map.Entry<Integer, Eindwerk>> entries = Eindwerken.entrySet();

        for( Map.Entry<Integer, Eindwerk> entry : entries ){
            if(entry.getValue().equals(eindwerk)){
                Eindwerken.remove(entry.getKey());
            }
        }

    }

    @Override
    public void voegToe(Eindwerk eindwerk) {
        if(! Eindwerken.containsValue(eindwerk)){
            Eindwerken.put(counter, eindwerk);
            counter++;
        }
    }
}
