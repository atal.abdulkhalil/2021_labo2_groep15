package eindwerkCollectie;

import java.util.ArrayList;
import java.util.TreeSet;

public class EindwerkCollectieSet implements EindwerkCollectieInterface{


    TreeSet<Eindwerk> eindwerks = new TreeSet<Eindwerk>();

    @Override
    public Eindwerk[] getEindwerkenVanOpleiding(String opleiding) {
        int teller = 0;
        Eindwerk[] eindwerk = new Eindwerk[eindwerks.size()];
        for (Eindwerk test: eindwerks){
            if (opleiding.equals(test.getOpleiding())){
                eindwerk[teller] = test;
            }
            teller++;
        }
        return eindwerk;
    }

    @Override
    public void verwijder(Eindwerk eindwerk) {

    }

    @Override
    public void voegToe(Eindwerk eindwerk) {
        eindwerks.add(eindwerk);
    }
}
