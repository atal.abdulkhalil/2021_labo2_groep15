package eindwerkCollectie;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;


class BufferedReaderReadLineExample {
    public static void main(String[] args) {

        ArrayList<Eindwerk> eindwerken;
        eindwerken = new ArrayList<Eindwerk>();
        String line = null;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("eindwerken.txt"))){
            while ((line = bufferedReader.readLine()) != null) {
                String[] lineParts = line.split("\$");
                if (lineParts.length > 0)
                {
                    Student student = new Student(lineParts[0], lineParts[1]);
                    Eindwerk test = new Eindwerk(lineParts[5], (Integer.parseInt(lineParts[4])), lineParts[3], student);
                    eindwerken.add(test);
                }
            }
            System.out.println(eindwerken);
        } catch (Exception e) {
            System.out.println(e.getMessage());
           /* e.printStackTrace();*/
        }

    }

}

